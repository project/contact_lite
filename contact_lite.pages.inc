<?php

/**
 * @file
 * Page callbacks for the contact_lite module main form.
 */


/**
 * Form constructor for the site-wide contact form.
 *
 * @see contact_lite_site_form_validate()
 * @see contact_lite_site_form_submit()
 * @ingroup forms
 * @throws \Exception
 */
function contact_lite_site_form($form, &$form_state) {
  global $user;

  // Check if flood control has been activated for sending e-mails.
  $limit = variable_get('contact_lite_threshold_limit', 5);
  $window = variable_get('contact_lite_threshold_window', 3600);

  if (!flood_is_allowed('contact_lite', $limit, $window) && !user_access('administer site configuration')) {
    drupal_set_message(t('You cannot send more than %limit messages in @interval. Try again later.', ['%limit' => $limit, '@interval' => format_interval($window)]), 'error');
    drupal_access_denied();
    drupal_exit();
  }

  if (!$user->uid) {
    $form['#attached']['library'][] = ['system', 'jquery.cookie'];
    $form['#attributes']['class'][] = 'user-info-from-cookie';
  }

  $form['#attributes']['class'][] = 'contact-form contact_lite-contact-form';
  $form['info']['#markup'] = '<p>' . t('Send a quick message using the form below.') . '</p>';
  $form['name'] = [
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => 255,
    '#default_value' => '',
    '#required' => TRUE,
    '#attributes' => [
      'required' => 'required',
    ],
  ];
  $form['mail'] = [
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#maxlength' => 255,
    '#default_value' => '',
    '#required' => TRUE,
    '#attributes' => [
      'required' => 'required',
    ],
  ];
  $form['message'] = [
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#required' => TRUE,
    '#attributes' => [
      'required' => 'required',
    ],
  ];
  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Send message'),
  ];

  contact_lite_add_form_captcha($form, $form_state);

  return $form;
}

/**
 * Form validation handler for contact_lite_site_form().
 *
 * @see contact_lite_site_form_submit()
 */
function contact_lite_site_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail', t('You must enter a valid e-mail address.'));
  }
  contact_lite_captcha_form_validate($form, $form_state);
}

/**
 * Form submission handler for contact_site_form().
 *
 * @see contact_site_form_validate()
 *
 * @param $form
 * @param $form_state
 */
function contact_lite_site_form_submit($form, &$form_state) {
  global $user;

  $values = $form_state['values'];
  $values['sender'] = clone $user;
  $values['sender']->name = $values['name'];
  $values['sender']->mail = $values['mail'];

  // Save the anonymous user information to a cookie for reuse.
  if (!$user->uid) {
    user_cookie_save(array_intersect_key($values, array_flip(['name', 'mail'])));
  }

  $account = user_load(1);

  // Get the to and from e-mail addresses.
  $to = $account->mail;
  $from = $values['sender']->mail;

  // Send the e-mail to the recipients using the site default language.
  drupal_mail('contact_lite', 'page_mail', $to, language_default(), $values, $from);

  flood_register_event('contact_lite', variable_get('contact_lite_threshold_window', 3600));
  watchdog('mail', '%sender-name (@sender-from) sent an e-mail..', ['%sender-name' => $values['name'], '@sender-from' => $from]);

  // Jump to home page rather than back to contact page to avoid
  // contradictory messages if flood control has been activated.
  drupal_set_message(t('Your message has been sent.'));
  $form_state['redirect'] = '';
}


/**
 * @param $form
 * @param $form_state
 *
 * @throws \Exception
 */
function contact_lite_add_form_captcha(&$form, &$form_state) {
  $recaptcha_api_key = variable_get('contact_lite_recaptcha_key', false);
  if (!$recaptcha_api_key) {
    return;
  }

  $form['captcha'] = [
    '#type' => 'hidden',
    '#attached' => [
      'js' => [
        'racaptcha' => [
          'data' => 'https://www.google.com/recaptcha/api.js',
          'defer' => TRUE,
          'preprocess' => FALSE,
          'external' => TRUE,
        ],
      ],
    ],
    '#suffix' => '<div class="g-recaptcha" data-sitekey="'.$recaptcha_api_key.'"></div>',
  ];
}

/**
 * @param $form
 * @param $form_state
 *
 * @return bool|void
 */
function contact_lite_captcha_form_validate($form, &$form_state) {
  $recaptcha_api_secret = variable_get('contact_lite_recaptcha_secret', false);

  if (!$recaptcha_api_secret) {
    return;
  }

  if (!empty($form_state['input']['g-recaptcha-response']) && is_string($form_state['input']['g-recaptcha-response']) && !isset($form_state['input']['g-recaptcha-response']{500})) {
    $response = drupal_http_request('https://www.google.com/recaptcha/api/siteverify', [
      'method' => 'POST',
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded'
      ],
      'data' => http_build_query([
        'secret' => $recaptcha_api_secret,
        'response' => $form_state['input']['g-recaptcha-response'],
      ])
    ]);

    if (isset($response->data)) {
      $data = json_decode($response->data);
      if (!empty($data->success)) {
        return;
      }
    }
  }

  form_set_error('captcha', t('Please complete the anti-bot verification.'));
}
